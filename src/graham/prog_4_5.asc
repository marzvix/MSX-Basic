10 REM *
20 REM * PROG 4.5 - Men Mowing Meadows
30 REM *
40 CLS
50 A$ = " One man and his dog
60 B$ = " went to mow a meadow"
70 PRINT " One man went to mow
80 PRINT " went to mow a meadow"
90 PRINT A$ : PRINT B$
100 FOR I=2 TO 10
110 REM * Delay Loop
120 FOR D = 1 TO 800:NEXT D
130 CLS
140 BEEP
150 PRINT I;" men went to mow
160 PRINT " went to mow a meadow"
170 FOR J=I TO 2 STEP -1
180 PRINT J;" men"
190 NEXT J
200 PRINT A$ : PRINT B$
210 NEXT
220 PRINT "The End"
230 END
